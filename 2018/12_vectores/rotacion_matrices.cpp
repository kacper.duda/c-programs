#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int  main (){

    double x,x1,x2,
           y,y1,y2,
           z,z1,z2,
           resultado;

    double A[3][3]{
        {x,y,z},
        {x1,y1,z1},
        {x2,y2,z2},

    };

    printf ("Dime las X: ");
    scanf  ("%lf %lf %lf",&x,&x1,&x2);
    printf ("Dime las Y: ");
    scanf  ("%lf %lf %lf",&y,&y1,&y2);
    printf ("Dime las R: ");
    scanf  ("%lf %lf %lf",&z,&z1,&z2);

    resultado = x*y1*z2 + x1*y2*z + x2*z1*y;


    printf ("%.0lf %.0lf %.0lf \n"
            "%.0lf %.0lf %.0lf \n"
            "%.0lf %.0lf %.0lf \n", x,x1,x2,y,y1,y2,z,z1,z2);

    printf ("%0.lf\n",resultado);

    return EXIT_SUCCESS;
}
