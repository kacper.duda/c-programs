#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    double X, Y, Z, r;

    printf("¿Cuánto mide X? : ");
    scanf(" %lf", &X);
    printf("¿Cuánto mide Y? : ");
    scanf(" %lf", &Y);
    printf("¿Cuánto mide Z? : ");
    scanf(" %lf", &Z);

    r=sqrt(pow(X,2)+pow(Y,2)+pow(Z,2));

    printf("El modulo es %.2lf\n", r);

    return EXIT_SUCCESS;
}
