#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define X 0
#define Y 1
#define DIM 2

double modulo(double vector[DIM]) {
    return sqrt(pow(vector[X],2) + pow(vector[Y],2));
}

int main(){
    double vector[2][DIM],
           modulo[2],
           producto = 0,
           angulo;

    system("clear");

    for (int i=0; i<2; i++) {
        printf("Vector %i:(X,Y) ", i + 1);
        scanf(" %lf, %lf", &vector[i][X], &vector[i][Y]);
    }
    printf("\n");

    for (int c=0; c<DIM; c++)
        producto += vector[0][c] * vector[1][c];

    angulo = acos(producto / modulo(vector)[0] / modulo(vector)[1]);
    angulo *= 180 / M_PI;

    printf("Ángulo = %.2lfº\n", angulo);

    return EXIT_SUCCESS;
}
