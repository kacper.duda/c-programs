
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265

int main(){

    double X, Y, r;
    double result;

    printf("¿Cuánto mide X? : ");
    scanf(" %lf", &X);
    printf("¿Cuánto mide Y? : ");
    scanf(" %lf", &Y);

    result = atan2(Y,X) * 180 / PI;

    printf("El ángulo es: %.2lfº\n", result);

    return EXIT_SUCCESS;
}
