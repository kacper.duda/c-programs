#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define E 0.000001

double fc(int base, int prof){
    if(prof == 1)
        return base;
    return base + 1. / fc(base, prof - 1);
}

int main(int argc, char *argv[]){
    int base, i;
    double result0 = 0, result1= -100;

    printf("Fracción continua del número: ");
    scanf(" %i", &base);

    for(i=1; fabs(result1-result0)>E && i<20; i++){
        result0 = result1;
        result1 = fc(base, i);
    }
    printf("Fracción continua(%i,%i) = %.4lf\n", base, i, result1);

    return EXIT_SUCCESS;
}
