#include <stdio.h>
#include <stdlib.h>

int factorial(int n) {
    if(n == 0)
        return 1;
    return n * factorial(n-1);
}

// Función punto de entrada
int main(int argc, char *argv[]){
    int valor;

    printf("Valor de n: ");
    scanf("%i", &valor);

    factorial(valor);

    printf(" %i \n", factorial(valor));

    return EXIT_SUCCESS;
}
