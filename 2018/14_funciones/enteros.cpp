
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 10

int num(int op[N]) {
    for(int i=0; i<N; i++)
        op[i] = pow(i,2);
}

// Función punto de entrada
int main(){
    int potencia[N];

    num(potencia);
    for(int i=0; i<N; i++)
        printf("La posición [%i] tiene la potencia %i\n", i, potencia[i]);

    return EXIT_SUCCESS;
}
