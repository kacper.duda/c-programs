#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int n;

    printf("Dime un número hexadecimal: ");
    scanf(" %x", &n);
    if(n%3==0)
        printf("El número hexadecimal %x es múltiplo de 3.\n", n);
    else
        printf("El número hexadecimal %x no es múltiplo de 3.\n", n);

    return EXIT_SUCCESS;
}
