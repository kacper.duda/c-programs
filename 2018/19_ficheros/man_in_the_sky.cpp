#include <stdio.h>
#include <stdlib.h>
#define NOMBRE "cancion.txt"

const char * song = "\n\
                Don't think sorry's easily said\n\
                Don't try turning tables instead\n\
                You've taken lots of chances before\n\
                But I ain't gonna give anymore\n\
\n\
                Don't ask me\n\
                That's how it goes\n\
                'Cos part of me knows what you're thinking\n\
\n\
                Don't say words you're gonna regret\n\
                Don't let fire rush to your head\n\
                I've heard the accusation before\n\
                And I ain't gonna take anymore\n\
\n\
                Believe me\n\
                The sun in your eyes\n\
                Made some of the lies worth believing\n\
\n\
";

void print_usage () {
    printf ("Esto se usa así\n");
}

void informo (const char *mssg) {
    print_usage ();
    fprintf (stderr, "%s\n", mssg);
    exit(1);
}

int main(int argc, char *argv[]) {
    FILE *fichero;

    if ( !(fichero = fopen ( NOMBRE, "w" )) )
        informo ("No se ha podido abrir el fichero.");
    fprintf(fichero, "%s", song);
    getchar ();
    fclose (fichero);

    return EXIT_SUCCESS;
}
