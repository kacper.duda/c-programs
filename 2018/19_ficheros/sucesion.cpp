#include <stdio.h>
#include <stdlib.h>

#define MAX         0x100 // 0x100 = 256
#define N           10
#define DUMP "binario.dat"

int main(int argc, char *argv[]) {
    int sucesion[N] = {1,1,2,3,5,8,13,21,34,55};
    FILE *pf;
    int n = sizeof(sucesion) / sizeof(int);

    if ( !(pf = fopen(DUMP, "wb")) ) {
        fprintf(stderr, "Ajín.\n");
        return EXIT_FAILURE;
    }

    fwrite (sucesion , sizeof(int), n, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}
