#include <stdio.h>
#include <stdlib.h>

#define N           10
#define DUMP "binario.dat"

int rellenar (int fi[N], int i) {
    if ( i == 1 ) {
        fi[0] == 1;
        return fi[1] == 1;
    }
    return fi[i] = rellenar(fi, i-1) + fi[i-2];
}

int main(int argc, char *argv[]) {
    FILE *pf;
    int sucesion[N];

    rellenar(sucesion, N-1);

    if ( !(pf = fopen(DUMP, "rb")) ) {
        fprintf(stderr, "Ajín.\n");
        return EXIT_FAILURE;
    }

    fwrite (sucesion , sizeof(int), N, pf);

    fclose(pf);

    return EXIT_SUCCESS;
}
