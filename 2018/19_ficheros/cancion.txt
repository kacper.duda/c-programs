
                Don't think sorry's easily said
                Don't try turning tables instead
                You've taken lots of chances before
                But I ain't gonna give anymore

                Don't ask me
                That's how it goes
                'Cos part of me knows what you're thinking

                Don't say words you're gonna regret
                Don't let fire rush to your head
                I've heard the accusation before
                And I ain't gonna take anymore

                Believe me
                The sun in your eyes
                Made some of the lies worth believing

