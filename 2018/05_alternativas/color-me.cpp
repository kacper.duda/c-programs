#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMARILLO 2
#define AZUL 1

// Función punto de entrada
int main(){

    char respuesta;
    int color = 0;

    /* Entrada de Datos */

    printf("¿Ves Rojo?\n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf("¿Ves Amarillo?\n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AMARILLO;

    printf("¿Ves Azul?\n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AZUL;

    switch(color){

        case 0:
            printf("- Ves Negro -\n");
            break;
        case 1:
            printf("- Ves Azul -\n");
            break;
        case 2:
            printf("- Ves Amarillo -\n");
            break;
        case 3:
            printf("- Ves Verde -\n");
            break;
        case 4:
            printf("- Ves Rojo -\n");
            break;
        case 5:
            printf("- Ves Morado -\n");
            break;
        case 6:
            printf("- Ves Naranja -\n");
            break;
        case 7:
            printf("- Ves Blanco -\n");
            break;

    }


    return EXIT_SUCCESS;
}
