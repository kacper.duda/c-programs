#include <stdio.h>
#include <stdlib.h>

#define TRIANGULO 1
#define CUADRADO 2
#define RECTANGULO 3
#define CIRCULO 4

// Función punto de entrada
int main(){

    int opcion;

    system("clear");
    system("figlet FIGURAS");

    printf( "\n"
            "\tSELECCIONE UNA FIGURA: \n"
            "\t----------------------\n"
            "\t\t1. Triángulo\n"
            "\t\t2. Cuadrado\n"
            "\t\t3. Rectángulo\n"
            "\t\t4. Círculo\n"
            "\n"
            "Opción = "
    );
    scanf(" %i", &opcion);

    switch(opcion){
        case TRIANGULO:
            system("figlet Triangulo");
            printf(" \n==> Triángulo <== \n");
            break;
        case CUADRADO:
            system("figlet Cuadrado");
            printf(" \n==> Cuadrado <== \n");
            break;
        case RECTANGULO:
            system("figlet Rectangulo");
            printf(" \n==> Rectángulo <== \n");
            break;
        case CIRCULO:
            system("figlet Circulo");
            printf(" \n==> Círculo <== \n");
            break;
        default:
            printf(" Solo puedes seleccionar del 1-4.\n");
            break;
    }

    return EXIT_SUCCESS;
}
