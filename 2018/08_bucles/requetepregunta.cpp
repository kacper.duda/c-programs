#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    char respuesta;

    do {
        printf( "\n¿Quieres que te cuente un cuento\nrecuento que nunca se acaba? (s/n)\n");
        scanf(" %c", &respuesta);
        if (respuesta != 'n')
        printf("Yo no te digo ni que sí ni que no,\nlo que digo es que si\n");
    } while (respuesta != 'n');

    printf("\nComo no hay que ser un pesado en esta vida: ADIOS, Isabel.\n");

    return EXIT_SUCCESS;
}
