#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define m 100000

// Colores
#define NORMAL        "\x1b[39m"
//
#define RESET_COLOR   "\x1b[0m"
#define NEGRO_T       "\x1b[30m"
#define NEGRO_F       "\x1b[40m"
#define ROJO_T        "\x1b[31m"
#define ROJO_F        "\x1b[41m"
#define VERDE_T       "\x1b[32m"
#define VERDE_F       "\x1b[42m"
#define AMARILLO_T    "\x1b[33m"
#define AMARILLO_F    "\x1b[43m"
#define AZUL_T        "\x1b[34m"
#define AZUL_F        "\x1b[44m"
#define MAGENTA_T     "\x1b[35m"
#define MAGENTA_F     "\x1b[45m"
#define CYAN_T        "\x1b[36m"
#define CYAN_F        "\x1b[46m"
#define BLANCO_T      "\x1b[37m"
#define BLANCO_F      "\x1b[47m"

// Función punto de entrada
int main(){

    int l = 15;

    system("clear");
    printf("\n\n");
    system("toilet FIGURITAS");
    printf("\n\n");

    // Cuadrado
    printf("- Cuadrado\n\n");
    for(int f=0; f<l; f++) {
        for(int c=0; c<l; c++)
            printf(AMARILLO_T" ⛋");
        usleep(m);
        printf("\n");
    }

    printf(NORMAL"\n\n");

    // Cuadrado sin relleno
    printf("- Cuadrado sin relleno\n\n");
    for(int f=0; f<l; f++) {
        for(int c=0; c<l; c++){
            if(f == 0 || c == 0 || f == l-1 || c == l-1 || f == c || f+c == l-1 ){
                printf(ROJO_T" ⛋");
            } else {
                printf("  ");
            }
        }
        usleep(m);
        printf("\n");
    }

    printf(NORMAL"\n\n");

    // Triángulo
    printf("- Triángulo\n\n");
    for(int f=0; f<l; f++) {
        for(int c=0; c<=f; c++)
            printf(MAGENTA_T" ⛔");
        usleep(m);
        printf("\n");
    }

    printf(NORMAL"\n\n");

    // Triángulo
    printf("- Triángulo sin relleno\n\n");
    for(int f=0; f<l; f++) {
        for(int c=0; c<=f; c++){
            if(f == 0 || c == 0 || f == l-1  || c == f ){
                printf(MAGENTA_T" ⛔");
            } else {
                printf("   ");
            }
        }
        usleep(m);
        printf("\n");
    }

    printf(NORMAL"\n\n");

    return EXIT_SUCCESS;
}
