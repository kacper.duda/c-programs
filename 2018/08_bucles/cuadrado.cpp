#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int l = 15;

    printf("\n\n");

    for(int f=0; f<l; f++) {
        for(int c=0; c<l; c++)
            printf(" *");
        printf("\n");
    }

    printf("\n\n");

    for(int f=0; f<l; f++) {
        for(int c=0; c<l; c++){
            if(f == 0 || c == 0 || f == l-1 || c == l-1){
                printf(" *");
            } else {
                printf("  ");
            }
        }
        printf("\n");
    }
    printf("\n\n");

    return EXIT_SUCCESS;
}
