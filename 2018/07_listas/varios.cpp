#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    unsigned primo[] = {2,3,5,7,11,13,17,19,23}; // Unsigned = que no tiene signo
    unsigned elementos = (unsigned) sizeof(primo) / sizeof(int); // este caso se llama molde
    unsigned *peeping = primo;
    char *tom = (char *) primo;
    unsigned **police = &peeping;

    printf( "PRIMO: \n"
            "======\n"
            " Localización (%p)\n"
            " Elementos: %u [%u..%u]\n" //lu = long unsigned int
            " Tamaño: %lu bytes.\n\n",
            primo,
            elementos,
            primo[0], primo[elementos-1],
            sizeof(primo));

    printf("0: %u\n", peeping[0]);
    printf("1: %u\n", peeping[1]);
    printf("0: %u\n", *peeping); // * = allí donde apunta
    printf("1: %u\n", *(peeping+1)); // mira la siguiente celda "*(peeping+1)" ARITMÉTICA DE PUNTEROS, suma de elemento en elemento y no de byte en byte
    printf("Tamaño: %lu bytes.\n", sizeof(peeping));
    printf("\n");

        // Memory Dump --> Volcado de Memoria
    for (int i=0; i<sizeof(primo); i++)
      printf("%02X", *(tom + i)); // %02X = rellena los espacios con 0 en hexadecimal
    printf("\n\n");

    printf("Police contiene %p\n", police);
    printf("Peeping contiene %p\n", *police);
    printf("Primo[0] contiene %u\n", **police);

    return EXIT_SUCCESS;
}
