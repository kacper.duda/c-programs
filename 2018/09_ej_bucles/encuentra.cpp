#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};
    int pos = -1;
    int buscado = 13;

    /* Buscar el 13 en A */
    for(int i=0; i<sizeof(A); i++)
        if(A[i] == buscado){
            pos = i;
            printf("La posición de %i es %i\n", buscado, i);
        }

    return EXIT_SUCCESS;
}
