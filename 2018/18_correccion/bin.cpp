#include <stdio.h>
#include <stdlib.h>

#define N 0x100
int res [N];

void push (int n) {
    static int i = 0;
    res[i++] = n;
}

void ultimo_bit (int dec) {
    if (dec) {
        ultimo_bit (dec / 2);
        printf("%i", dec % 2);
        push (dec % 2);
    }
}

int main(int argc, char *argv[]) {

    ultimo_bit(9);
    printf("\n");
    printf("Array: [");
    for (int i=0; i<N; i++)
        printf("%i", res[i]);
    printf("]\n");

    return EXIT_SUCCESS;
}
