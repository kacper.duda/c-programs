#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio_ext.h>

#define MAX 0x100
#define LMUERTE 10

// COLORES
#define ROJO "\x1B[31m"
#define VERDE "\x1B[32m"
#define AZUL "\x1B[34m"
#define NEGRO "\x1B[30m"
#define BLANCO "\x1B[37m"
#define MAGENTA "\x1B[35m"
#define CYAN "\x1B[36m"
#define AMARILLO "\x1B[33m"

bool es_mala (char letra) {
    return true;
}

int main (int argc, char *argv[]) {

    char letra;
    char *malas, *letras;
    int total_malas = 0, total_letras = 0;

    malas = (char *) malloc (LMUERTE + 1);
    letras = (char *) malloc (MAX + 1);
    bzero (malas, LMUERTE + 1);
    bzero (letras, MAX + 1);

    while (total_malas < LMUERTE) {
        system ("clear");
        system ("toilet -f pagga AHOGADO");
        printf ("\n");
        printf ("Letras: " VERDE "%s" BLANCO "\n", letras);
        printf ("Malas: " ROJO "%s" BLANCO "\n", malas);
        printf ("================================\n\n");


        printf (CYAN "Letra: ");
        letra = getchar ();
        __fpurge (stdin);
        printf (BLANCO"\n");
        if (strchr (letras, letra))
            continue;

        if (es_mala (letra))
            *(malas + total_malas++) = letra;
        *(letras + total_letras++) = letra;
    }

    free (letras);
    free (malas);

    return EXIT_SUCCESS;
}
