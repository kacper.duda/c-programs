#include <stdio.h>
#include <stdlib.h>

/*
 * Pedir frase al usuario
 * Guardarla en memoria
 * Con otro puntero sustituir cada caracter de la frase por la letra+3 (abecedario)
 * Imprimir
 * Liberar memoria
*/

int main(int argc, char *argv[]) {
    char *frase;
    char *p;
    printf ("¿ Cuáles son tus ordenes Julio ? ");
    scanf (" %m[^\n]", &frase);

    for (char *p=frase; *p!='\0'; p++)
        *p = *p + 3;

    printf("%s\n", frase);

    printf ("\n");

    free (frase);

    return EXIT_SUCCESS;
}
