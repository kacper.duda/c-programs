#include <stdio.h>
#include <stdlib.h>

int encuentra(int buscado, int A[], int n_elem){
    int pos = -1;

    // Busca la celda de un valor
    for(int i=0; i<n_elem; i++)
        if(A[i] == buscado)
            pos = i;

    return pos;
}

// Función punto de entrada
int main(){

    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};
    int buscado = 13;
    int pos = encuentra (buscado, A, sizeof(A)/sizeof(int));

    /* Buscar el 13 en A */
    if (pos>0)
        printf("%i está en %i.\n", buscado, pos);
    else
        printf("Nota!\n");

    return EXIT_SUCCESS;
}
