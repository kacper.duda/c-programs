#include <stdio.h>
#include <stdlib.h>

#define N 1000
// Función punto de entrada
int main(){

    int n_alumnos = 0;
    double nota[N],
           entrada,
           media = 0;

    system("clear");

    // ENTRADA DE DATOS
    do {
    printf("Dime tu nota: ");
        scanf(" %lf", &entrada);
        if (entrada>=0)
            nota[n_alumnos++] = entrada;
    } while (entrada>=0);

    // CÁLCULOS
    for (int i=0; i<n_alumnos; i++)
        media += nota[i];

    media /= n_alumnos;

    // SALIDA DE DATOS
    printf("La nota media de la clase es: %.2lf\n", media);

    return EXIT_SUCCESS;
}
