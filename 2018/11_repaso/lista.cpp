
#include <stdio.h>
#include <stdlib.h>

#define N 10
// Función punto de entrada
int main(){
    int cadena[N];

    system("clear");

    /* ENTRADA DE DATOS */

    /* CÁLCULOS */
    for (int i=0; i<N; i++)
        cadena[i] = i + 1;

    /* SALIDA DE DATOS */
    for (int i=0; i<N; i++)
        printf("%i ", cadena[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
