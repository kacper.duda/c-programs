
#include <stdio.h>
#include <stdlib.h>

#define N 10
// Función punto de entrada
int main(){
    int cadena[N];

    system("clear");

    for (int i=0; i<N; i++)
        cadena[i] = i + 1;

    printf("\n");

    return EXIT_SUCCESS;
}
