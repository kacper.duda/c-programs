#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    printf("%c\n", 97);
    printf("%c\n", 0x61);
    printf("%c\n", 'a');

    printf("%i\n", 97);

    puts("");

    int numero;
    printf("Num: ");
    scanf(" %i", &numero);
    printf("Numero => [%p]: %i\n", &numero, numero); // Te indica en qué celda de la memoria se encuentra el valor
    printf("Linea %i\n", 47);

    int dia, annio;
    printf("Nacimiento dd/mm/aaaa: ");
    scanf(" %i/%*i/%i", &dia, &annio); // Carácter de supresión de asignación "*"

    char hex[32];
    scanf(" %[0-9a-fA-F]", hex);
    scanf(" %[^0-9a-fA-F]", hex);

    return EXIT_SUCCESS;
}
