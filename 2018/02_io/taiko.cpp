#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Batería creada con sonidos de ubuntu desde terminal "\a" */

// Función punto de entrada
int main(){

    /* printf tiene escritura bufferizada */
    // puts("hola");
    // printf("hola\n");
    // printf("Hola, %s. ¿Qué tal estás? %Xh\n", "Pepe", (int) '0');
    fprintf(stderr, "\a"); /* stderr no está bufferizado */
    usleep(100000);
    fputc('\a', stderr);
    usleep(100000);
    printf("\a\n");

    return EXIT_SUCCESS;
}
