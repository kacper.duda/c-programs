
#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int dn,mn,an; // Día,Mes,Año de Nacimiento
    int dh,mh,ah; // Días,Mes,Año de HOY
    int dias;

    printf("Fecha de Nacimiento (dd/mm/aaaa): ");
    scanf(" %d/%d/%d",&dn,&mn,&an);
    printf("Fecha de Hoy (dd/mm/aaaa): ");
    scanf(" %d/%d/%d",&dh,&mh,&ah);

    dias = dh - dn + (mh - mn) * 30 + (ah - an) * 365;

    printf("\nHas vivido %d dias\n", dias);

    return EXIT_SUCCESS;
}
