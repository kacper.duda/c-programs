#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){

    int numero;

    printf("¿Qué número quieres cambiar a hexadecimal? ");
    scanf(" %i", &numero);
    printf("El número en hexadecimal es 0x%x\n", numero);

    return EXIT_SUCCESS;
}
