#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int main(){
    int i, numero;
    char titulo[0x100];

    system("clear");
    printf("\n\n");
    system("toilet -fpagga --metal Tabla de Multiplicar");
    printf("\n\n Introduce el número de la tabla de multiplicar que desee: ");
    scanf("%i", &numero);
    system("clear");
    printf("\n\n");
    sprintf(titulo,"figlet -w 120 -f future Tabla de Multiplicar del %i :", numero);
    system(titulo);

    for ( i = 1 ; i <= 10 ; i++  ) {
        printf( "\n\n  %i x %i = %i ", i, numero, i * numero  );
    }

    printf("\n\n");

    return EXIT_SUCCESS;
}
