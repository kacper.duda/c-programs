
#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int tabla){ /* Parámetro Formal */
    char titulo[MAX];
    sprintf(titulo, "toilet -fpagga --metal Tabla del %i", tabla);
    system(titulo);
}

// Función punto de entrada
int main(){
    //Declaración de variables
    int tabla, op1 = 1;

    //Menu
    system("clear");
    printf("\n\n");
    printf("¿Qué tabla quieres?\n");
    printf("Tabla: ");
    scanf("%i", &tabla);
    printf("\n\n");

    //Título
    pon_titulo(tabla); //Llamada con parámetro actual

    //Resultado
     for ( op1 = 1 ; op1 <= 10 ; op1++) {
         printf( "\n\n  %i x %i = %i ", op1, tabla, op1 * tabla);
     }

     printf("\n\n");

    return EXIT_SUCCESS;
}
